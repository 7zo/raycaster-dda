public class Player extends Entity {
    private int speed;
    public double angle;
    public double xDir;
    public double yDir;
    private double nextX;
    private double nextY;

    public Player(Game game, int x, int y) {
        super(game, x, y);
        speed = 1;
        angle = 0.1;
        xDir = 0;
        yDir = 0;
    }

    public void tick() {
        if (angle < 0) angle += 2 * Math.PI;
        if (angle > 2 * Math.PI) angle -= 2 * Math.PI;
        xDir = Math.cos(angle);
        yDir = Math.sin(angle);
        if (game.getInput().up) {
            nextX = x + (speed * xDir);
            nextY = y + (speed * yDir);
        }
        if (game.getInput().down) {
            nextX = x - (speed * xDir);
            nextY = y - (speed * yDir);
        }
        if (game.getInput().a) {
            nextX = x + (speed * yDir);
            nextY = y - (speed * xDir);
        }
        if (game.getInput().d) {
            nextX = x - (speed * yDir);
            nextY = y + (speed * xDir);
        }
        if (game.getInput().left) {
            angle -= Math.PI / 64;
        }
        if (game.getInput().right) {
            angle += Math.PI / 64;
        }

        if (map[(int) y/game.getCellSize()][(int) nextX/game.getCellSize()] == 0) {
            x = nextX;
        }

        if (map[(int) nextY/game.getCellSize()][(int) x/game.getCellSize()] == 0) {
            y = nextY;
        }

        //nextX = 0;
        //nextY = 0;
    }
}
